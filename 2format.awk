# sort list
cat *.log | awk '/V/ {if ($0 !~ /undefined/) {
  gsub(/OOTO/, "");
  print $2 "\t" $3 "\t" $4 " " $5 "\t" $6 " " $7}
}' > names.txt

# delete the *.log file
rm *.log
