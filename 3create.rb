# get date for title
date = Time.now
ymd_hm = "#{date.year}-#{date.month}-#{date.day}_#{date.hour}#{date.min}"

# create and name file
chart_file = File.new("#{ymd_hm}.html", 'w')

names_raw = File.readlines('names.txt')
File.delete('names.txt')

names_formatted = []
names_raw.each do |n|
  names_formatted << n.gsub("\t", ' ').gsub("\n", '').chomp
end

# get time values for: var data = {values}
values = []
names_formatted.each do |t|
  if t.match(/[0-9]+\s[a-z]+/)[0].match(/[a-z]/)[0] == 'd'
    values << 8
  elsif t.match(/[0-9]+\s[a-z]+\s[0-9]/) != nil
    values << t.match(/[0-9]+\s[a-z]+/)[0].match(/[0-9]/)[0].to_i + 0.5
    #t.match(/[0-9]+\s[a-z]+\s[0-9]+/)[0].split(' ')[2].to_i # get minutes as int
  else
    values << t.match(/[0-9]+\s[a-z]+/)[0].match(/[0-9]/)[0].to_i
  end
end

# format name values for: var data = {lables}
c = 0
len = names_formatted.length
names = []
while c < len do
  names << names_formatted[c].match(/[A-Za-z\s]+/)[0]
  c += 1
end

chart_file.write('
<!DOCTYPE html>
<html>
  <head>
    <title>OOTO</title>
    <!-- Plotly.js -->
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
    <!-- Numeric JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>
  </head>
  <body>
    <center><h3>')
      chart_file.write("#{ymd_hm}")
    chart_file.write('</h3></center>
    
    <div id="chart"></div>
    <script>
      var data = [{
        values: ')
          chart_file.write("#{values}")
        chart_file.write(',
        labels: ')
          chart_file.write("#{names}")
        chart_file.write(',
        type: "pie"
      }];
        
      Plotly.newPlot("chart", data);
    </script>
  </body>
</html>
')

chart_file.close
