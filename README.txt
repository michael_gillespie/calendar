what does it do?
calendar scrapes outlook.com caleandar appointments and creates a pie chart
showing the percentage of time that task takes up out of the particular day.

skills used:
> JS
> awk
> ruby

requirements:
> web browser (tested on chrome)
> awk (gawk included with any linux OS)
> ruby

how to run:
1. open outlook web calendar and select a day
2. open chrome console and paste contents of: 1.scrape.js
3. r-click and save the outlook.office.com-[0-9]+.log file into the program dir
4. run awk script: ./2format (removes the *.log file)
5. run ruby script: ruby 3create.rb" (removes the names.txt file)
